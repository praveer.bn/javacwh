package com.company;

import java.util.Scanner;

public class Chapter5_ForLoop {
    public static void main(String[] args) {
//        int n =  10;
//        for(int i=0;i<(n/2);i++){
//            System.out.println(2*i+1);
//        }
        Scanner sc=new Scanner(System.in);
        System.out.println("enter the number");
        int n=sc.nextInt();
        for(int i=n;i!=0;i--){
            System.out.println(i);
        }
    }
}
