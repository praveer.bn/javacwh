package com.company;
class Myemployee{
    private int id;
    private  String name;
    public String getName(){
        return  name;
    }
    public void setName(String n){
        name=n;
    }
    public int getId(){
        return  id;
    }
    public void setId(int a){
        id=a;
    }
}
public class Chapter9_acc_get_set {
    public static void main(String[] args) {
        Myemployee praveer=new Myemployee();
        praveer.setId(11191);
        System.out.println(praveer.getId());
        praveer.setName("Praveer B N");
        System.out.println(praveer.getName());
    }
}
