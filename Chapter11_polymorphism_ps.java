package com.company;
abstract class Pen{
    abstract  void write();
    abstract  void refill();

}
class FountainPen extends Pen{
    void write(){
        System.out.println("writing");
    }


    void refill() {
        System.out.println("refilling");
    }
    void changenib(){
        System.out.println("changing nib");
    }

}
class Monkey{
    void jump(){
        System.out.println(" jumping monkey");
    }
    void bite(){
        System.out.println(" bitting");
    }
}
interface Basicanimal{
    void eat();
    void sleep();
}
class Human extends Monkey implements Basicanimal{
    void speak(){
        System.out.println("hello sir");
    }

    @Override
    public void eat() {
        System.out.println("eating");
    }

    @Override
    public void sleep() {
        System.out.println("sleeeping");
    }
}

public class Chapter11_polymorphism_ps {
    public static void main(String[] args) {
//      Q1 and Q2
        FountainPen pen =new FountainPen();
        pen.changenib();

//        Q3
        Human pra=new Human();
        pra.sleep();

    }
}
