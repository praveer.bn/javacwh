package com.company;

public class Chapter5_DowhileLoop {
    public static void main(String[] args) {
        int i=100;
        do{
            System.out.println(i);
            i++;
        }while (i<11);
    }
}
