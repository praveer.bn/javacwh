package com.company;
class base1{
    public base1() {
        System.out.println(" i am a constuctor of base class");
    }

    public base1(int a,int b) {
        System.out.println(" i am a constuctor of base class   "+ a);
    }
}
class derived1 extends base1 {
    public derived1() {
//        super(0);
        System.out.println("i am a constuctor of derived  class");
    }
    public derived1(int a,int b) {
//        super(a);
        System.out.println("i am a constuctor of derived  class"+ a+" "+b);
    }

}
    class derived2 extends derived1 {
public derived2() {
//        super();
        System.out.println("i am a constuctor of derived 2  class");
        }
public derived2(int a,int b,int c) {
        super(a,b);
        System.out.println("i am a constuctor of derived  class "+ a+" "+b+" "+c);
        }

        }
public class Chapter10_constructor_in_Inheritance {
    public static void main(String[] args) {
        //base1 b=new base1();
        derived1 d=new derived1(2,2);
//        derived2 e=new derived2();

    }
}
