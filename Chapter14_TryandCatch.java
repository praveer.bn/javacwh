package com.company;

import java.util.Scanner;

public class Chapter14_TryandCatch {
    public static void main(String[] args) {
//        int a = 900;
//        int b = 0;
////with try
//        try {
//            int c = a / b;
//            System.out.println("the result is " + c);
//        } catch (ArithmeticException d) {
//            d.fillInStackTrace();
//            System.out.println("the reason is "+d);
//        }
//        System.out.println("end");

//        handling the exception
        int [ ] array=new int[3];
        array[0]=20;
        array[1]=30;
        array[2]=40;
        Scanner sc=new Scanner(System.in);
        System.out.println("enter the index");
        int ind= sc.nextInt();
        System.out.println("enter number u  wanna div with");
        int num=sc.nextInt();
try {
    System.out.println("the value is "+ array[ind] );
    System.out.println("the value is " + array[ind] / num);
}

catch (ArithmeticException e){
    System.out.println("ArithmeticException exception occurd");
    System.out.println(e);
}
catch (ArrayIndexOutOfBoundsException e){
    System.out.println("ArrayIndexOutOfBoundsException exception occurd");
    System.out.println(e);
}
catch (Exception e){
    System.out.println("some other exception occurd");
    System.out.println(e);
}
//        System.out.println("*********************************");
    }
}
