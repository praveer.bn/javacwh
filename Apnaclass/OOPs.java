package com.company.Apnaclass;
class Student{
    int age;
    String name;
    public void getInfo(){
        System.out.println(this.age);
        System.out.println(this.name);
    }
//    non parameterized constructor
//    Student(){
//        System.out.println(("constructor called"));
//    }
//parameterized constructor
//    Student(String name,int age){
//    this.name="abhi";
//    this.age=22;
//   copy constructor
    Student(Student s2){
        this.name=s2.name;
        this.age= s2.age;
    }
    Student(){}
}
public class OOPs {
    public static void main(String[] args) {
        Student s1=new Student();
        s1.age=21;
        s1.name="Praveer";

        Student s2=new Student(s1);
        s2.getInfo();
    }
}
