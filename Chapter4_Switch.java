package com.company;

import java.util.Scanner;

public class Chapter4_Switch {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int var=sc.nextInt();
        switch(var){
            case 0:
                System.out.println("hiii");
                break;
            case 1:
                System.out.println("ok");
                break;
            case 2:
                System.out.println("hello");
                break;
            default:
                System.out.println("bye");

        }
    }
}
