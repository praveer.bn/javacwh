package com.company;

import java.util.ArrayList;
import java.util.Queue;
import java.util.Set;

public class Chapter15_CollectionArraylist {
    public static void main(String[] args) {
        ArrayList<Integer> l1=new ArrayList<>();
        ArrayList<Integer> l2=new ArrayList<>();
        l2.add(20);
        l2.add(21);
        l2.add(22);
        l1.add(4);
        l1.add(8);
        l1.add(6);
        l1.add(10);
        l1.add(12);
//        l1.remove(1);
        l1.add(14);
        l1.add(14);

        l1.add(1,17);
        l1.addAll(0,l2);
//        l1.clear();
        System.out.println(l1.contains(14));
        System.out.println(l1.indexOf(14));
        System.out.println(l1.lastIndexOf(14));
        l1.set(0,11111111);
//        for (int i = 0; i < l1.size(); i++) {
//            System.out.println(l1.get(i));
//
//        }
        System.out.println(l1.toString());
//    }


}}
