package com.company;

import java.util.Scanner;

public class Chapter6_Array {
    public static void main(String[] args) {
//        int array[] = new int[5];//first type
//        int [] array;
//        array=new int[5];// second type
//        array[0]=100;
//        array[1]=200;
//        array[2]=300;
//        array[4]=400;
//        array[4]=500;
        int array[]={1,2,3,4,5,6,7};// third type
//        Scanner sc = new Scanner(System.in);
//        System.out.println(" enter the array values");
//        for (int i = 0; i < array.length; i++) {
//            array[i] = sc.nextInt();
//        }
//
//        System.out.print("array=[");
//        for (int i = 0; i < array.length; i++) {
//
//            System.out.print(array[i] + ",");
//
//        }
//        System.out.print("]");

//        displaying array in reverse order
        for(int element:array){
            System.out.println(element);
        }
        for(int i= array.length-1;i>=0;i--){
            System.out.println(array[i]);
        }
    }
}
