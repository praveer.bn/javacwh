package com.company;

import java.util.Scanner;

class Myexception extends Exception{
    @Override
    public String getMessage() {
        return super.getMessage()+" i am getmsg";
    }

    @Override
    public String toString() {
        return " i am tostring";
    }



}

public class Chapter14_Exception_Class {
    public static void main(String[] args) {
//        Myexception myexception=new Myexception();
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        if (a<9){try{
            throw new Myexception();
        }catch (Exception e){
            System.out.println(e.getMessage());
            System.out.println(e.toString());
            System.out.println(e);
            System.out.println("done");
        }

        }
    }
}
