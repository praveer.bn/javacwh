package com.company;

import java.util.Scanner;

public class Chapter14_NestedTryCatch {
    public static void main(String[] args) {
        int [ ] array=new int[3];
        array[0]=20;
        array[1]=30;
        array[2]=40;
        Scanner sc=new Scanner(System.in);
        boolean flag=true;
            while (flag) {
                System.out.println("enter the index");
                int ind = sc.nextInt();

                try {
                    try {
                        System.out.println(" the value at ind is " + array[ind]);
                        flag =false;
                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.out.println("ArrayIndexOutOfBoundsException");
                        System.out.println("level 2");
                    }
                } catch (Exception e) {
                    System.out.println(e);
                    System.out.println("level 1");
                }
            }
        System.out.println("programm is over");
    }
}
