package com.company;
interface Camera{
    default void takesnap(){
        System.out.println("its snap");
    }
    void record();
}
interface wifi{
     String[] getNetworks() ;
    void connectnetwork();
}
class cellphone{
    public void call(){
        System.out.println("ting ting");
    }
    public void connect(){
        System.out.println("conecting......");
    }
}
class smartphone extends cellphone implements Camera,wifi{
    public void takesnap(){
        System.out.println("its snap........");
    }
    public void record(){
        System.out.println("its recording");
    }
    public String[] getNetworks(){
        String [] sr={"a","b","c"};
        return sr;
    }
    public void connectnetwork(){
        System.out.println("ok");
    };
}
public class Chapter11_defaultandPrivate {
    public static void main(String[] args) {
        smartphone sp=new smartphone();
        String[] sr= sp.getNetworks();
        for (String item:sr
             ) {
            System.out.println(item);
        }
    }
}
