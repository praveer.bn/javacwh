package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Chapter6_ps_array {
    public static void main(String[] args) {
        //Question 1
        /*
        float array[]={10.3f,20.2f,30.2f,40.2f,50.2f};
        float sum=0;
//        for(int i=0;i<array.length;i++){
//            sum=array[i]+sum;
        for(float a:array){
            sum=sum+a;
        } System.out.println(sum);
        */

        //question 2
        //method 1
        /*
        float array[] = {10.3f, 20.2f, 30.2f, 40.2f, 50.2f};
        Scanner sc = new Scanner(System.in);
        float a = sc.nextFloat();
        boolean yes = false;
        int b=0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == a) {
                yes = true;
                b=i;
            }}if(yes){
            System.out.println("enter number is in array at the index "+b);
        }else{
            System.out.println("enter number is not in array ");
        }*/
        //methode 2
        /*
        float array[] = {10.3f, 20.2f, 30.2f, 40.2f, 50.2f};
        Scanner sc = new Scanner(System.in);
        float a = sc.nextFloat();
        boolean yes = false;
        int b=0;
        for (float c:array){
            if (a==c) {
                yes = true;
                break;
            }}if(yes){
            System.out.println("enter number is in array ");
        }else{
            System.out.println("enter number is not in array ");
        }*/
        //question 3
        /*
        float array[]={10.3f,20.2f,30.2f,40.2f,50.2f};
        float sum=0;
//        for(int i=0;i<array.length;i++){
//            sum=array[i]+sum;
        for(float a:array){
            sum=sum+a;
        } System.out.println("the avg marks is "+sum/ array.length);

         */
        //Question 4
        /*
        int [][] mat1 = {{1, 2, 3},
                        {4, 5, 6}};
        int [][] mat2 = {{2, 6, 13},
                         {3, 7, 1}};
        int [][] result = {{0, 0, 0},
                {0, 0, 0}};

        for (int i=0;i<mat1.length;i++){ // row number of times
            for (int j=0;j<mat1[i].length;j++) { // column number of time
//                System.out.format(" Setting value for i=%d and j=%d\n", i, j);
                result[i][j] = mat1[i][j] + mat2[i][j];
                System.out.print(result[i][j] + " ");
            }
            System.out.println("");
        }

        // Printing the elements of a 2-D Array
//        for (int i=0;i<mat1.length;i++){ // row number of times
//            for (int j=0;j<mat1[i].length;j++) { // column number of time
//                System.out.print(result[i][j] + " ");
//                result[i][j] = mat1[i][j] + mat2[i][j];
            }
//            System.out.println(""); // Prints a new line
//        }

    }

         */
        //question 5
//    int[] a={1,2,3,4,5,6,7,8,9};
//    int l=a.length;
//    int temp;
//    int b=Math.floorDiv(l,2);
//    for(int i=0;i<b;i++) {
//        //swaping
//        //|A| |B| |temp|
//        temp = a[i];
//        a[i] = a[l - i - 1];
//        a[l - i - 1] = temp;
//    }
//        for (int i = 0; i < l; i++) {
//            System.out.print(a[i]+ " ");
//        }
         //question 6
//        int[] a={1,2,3,4,5,6,77,8,9};
//        int max=0;
//        for(int e:a){
//            if(e>max){
//                max=e;
//            }
//        }
//        System.out.println("the max value of element in  arry is "+max);
        //question 7
//        int[] a={1,2,3,4,5,6,-77,0,9};
//        int min=Integer.MAX_VALUE;
//        for(int e:a){
//            if(e<min){
//                min=e;
//            }
//        }
//        System.out.println("the max value of element in  arry is "+min);
        //question 8
        int[] a={1,972,123,4,599,632,-77,0,9};
        boolean b=true;
        for (int i = 0; i < a.length-1; i++) {
            if(a[i]>a[i+1]){
                b=false;
                break;
            }}if(b){
                System.out.println("sorted");
            }else{
                System.out.println("not sorted");
            }

        }
        }


