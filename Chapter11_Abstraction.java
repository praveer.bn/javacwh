package com.company;
abstract class Parent2{
    public Parent2(){
        System.out.println("i am constructor of p");
    }
    public void sayHello(){
        System.out.println("Hello");
    }
    abstract void greet();
}
class Child2 extends Parent2{
    public void greet(){
        System.out.println("greeting");
    }
}
public class Chapter11_Abstraction {
    public static void main(String[] args) {
        Parent2 parent2=new Child2();
        parent2.greet();
    }
}
