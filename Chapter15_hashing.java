package com.company;

import java.util.ArrayList;
import java.util.HashSet;

public class Chapter15_hashing {
    public static void main(String[] args) {
        ArrayList<Integer> l1=new ArrayList<>();
        l1.add(4);
        l1.add(8);
        l1.add(6);
        l1.add(10);
        l1.add(12);
//        l1.remove(1);
        l1.add(14);
        l1.add(14);
        HashSet<Integer> myHS=new HashSet<>(6,0.5f);
//        myHS.add(1);
//        myHS.add(2);
//        myHS.add(2);
//        myHS.add(3);
        myHS.addAll(l1);
        System.out.println(myHS);
    }
}



/*
HASHING
f(x)=x%10;
hash map
hash table
hash set
linkedlist hashmap

hash collision==openadress,chaining

*********************************************************
HASH SET
set of non repeting items
 */

