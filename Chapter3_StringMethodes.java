package com.company;

public class Chapter3_StringMethodes {
    public static void main(String[] args) {
        String name="Harry";
//        System.out.println(name.length());

//        System.out.println(name.toUpperCase());
//        System.out.println(name.toLowerCase());

//        String nontrim="    Harry     i       ";
//        System.out.println(name);
//        String trim=nontrim.trim();
//        System.out.println(trim);

//        System.out.println(name.substring(2));
//        System.out.println(name.substring(1,5));

//        System.out.println(name.replace('r','p'));
//        System.out.println(name.replace("rry","eir"));
//        System.out.println(name.replace("r","eir"));
//
//        System.out.println(name.startsWith("harr"));
//        System.out.println(name.endsWith("ry"));
//        System.out.println(name.charAt(0));

//        System.out.println(name.indexOf('r'));
//        System.out.println(name.indexOf('r',4));
//        System.out.println(name.lastIndexOf('r'));
//        System.out.println(name.lastIndexOf('r',5));

//
//        String newname="HarrY";
//        System.out.println(name.equals(newname));
//        System.out.println(name.equalsIgnoreCase(newname));
//
//        System.out.println("i am escape sequence \"double quote");


    }
}
