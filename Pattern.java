package com.company;

public class Pattern {
    public static void main(String[] args) {
//        pattern2(4);
//        pattern1(4);
//        pattern3(4);
//        pattern4(5);
//        pattern5(5);
        pattern28(5);
    }
    static void pattern2(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
    static void pattern1(int m){
                for(int i=1;i<=m;i++){
                    for(int j=1;j<=m;j++){
                        System.out.print("* ");
                    }
                    System.out.println();
        }
    }
    static void pattern3(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <=n+1-i; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
    static void pattern4(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <=i; j++) {
                System.out.print(j);
            }
            System.out.println();
        }
    }
    static void pattern5(int n) {
        for (int i = 0; i < 2*n; i++) {
            int a=i>n ? 2*n-i:i;
            for (int j = 0; j < a; j++) {

                System.out.print("* ");
            }
            System.out.println();
        }
    }
    static void pattern28(int n) {
        for (int i = 0; i < 2*n; i++) {
            int a=i>n ? 2*n-i:i;
            int b=n-a;
            for(int s=0;s<b;s++){
                System.out.print(" ");
            }
            for (int j = 0; j < a; j++) {

                System.out.print("* ");
            }
            System.out.println();
        }
    }
}

