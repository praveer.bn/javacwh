package com.company;

import javax.sound.midi.Soundbank;

class base{
    int x;
    public int getX() {
        System.out.println(" i am base and getter");
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }



}
class derived extends base{
    int y;

    public int getY() {
        System.out.println(" i am derived and getter");
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
public class Chapter10_Inheritance {
    public static void main(String[] args) {


//    creating the obj of base
        base b=new base();
        b.setX(4);
        System.out.println(b.getX());

//        creating the obj of derived
        derived d=new derived();
        d.setX(5);
        d.setY(6);
        System.out.println(d.getX());
        System.out.println(d.getY());

}}
