package com.company;

public class Chapter7_recursion {
    static int sum(int ...arr){
        int res=0;
        for( int e:arr){
            res +=e;
        }
        return res;
    }
    static int summ(int x,int ...arr){

        int res=x;
        for( int e:arr){
            res +=e;
        }
        return res;
    }
    static int fact(int x){
        if((x==0)||(x==1)){
            return 1;
        }else{
            return x*fact(x-1);
        }
    }
    static int facttt(int x){
        if((x==0)||(x==1)){
            return 1;
        }else{
            int p=1;
            for (int i=1;i<=x;i++){
                p *=i;
            }
            return p;
        }
    }
    static int fab(int x) {

        int a = 0, b = 1, c = 1;
        if (x == 0) {
            return 0;
        } else {

            for (int i = 0; i < x - 1; i++) {
                c = a + b;
                a = b;
                b = c;
            }
            return c;

        }
    }

    public static void main(String[] args) {
//        System.out.println(summ(1,2));
//        System.out.println(summ(11,1));
        System.out.println(fab(3));
//        System.out.println(facttt(5));
    }
}
